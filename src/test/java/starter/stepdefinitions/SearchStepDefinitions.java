package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("User calls endpoint {string}")
    public void userCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("title", contains("mango")));
    }
    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedFor(String productName) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("title", contains(productName)));
    }

    @Then("Verifies the statusCode {int}")
    public void VerifiesTheStatusCode(int arg0) {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("Verifies the invalidstatusCode {int}")
    public void VerifiesTheInvalidStatusCode(int arg0) {
        restAssuredThat(response -> response.statusCode(404));
    }


    @And("verifies details are not null")
    public void verifiesDetailsAreNotNull() {
        restAssuredThat(response -> response
                .assertThat().body("provider", notNullValue())
                .assertThat().body("title", notNullValue())
                .assertThat().body("url", notNullValue())
                .assertThat().body("brand", notNullValue())
                .assertThat().body("price", notNullValue())
                .assertThat().body("unit", notNullValue())
                .assertThat().body("isPromo", notNullValue())
                .assertThat().body("promoDetails", notNullValue())
                .assertThat().body("image", notNullValue()));
    }

    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {

        restAssuredThat(response -> response.statusCode(404)
                .body("detail.error",equalTo(true)) );
        restAssuredThat(response -> response
                .body("detail.message",equalTo("Not found")));
    }


}

