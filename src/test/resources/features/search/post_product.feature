Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  @Positive
  Scenario: Verify Correct Url For Apple
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then Verifies the statusCode 200

  @Positive
  Scenario: Verify Correct Url For Mango
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then Verifies the statusCode 200

  @Positive
  Scenario: Verify Correct Url For Tofu
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then Verifies the statusCode 200

  @Positive
  Scenario: Verify Correct Url For Water
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
    Then Verifies the statusCode 200

  @Positive
  Scenario: Verifies that the details for product are Not Null.
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then Verifies the statusCode 200
    And verifies details are not null

  @Negative
  Scenario: Search product with invalid product name
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then he does not see the results

  @Negative
  Scenario: Verify Orange is not a valid product
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/orange"
    Then Verifies the invalidstatusCode 404

# can't test the below scenarios as the Api Url data is changed everytime on refresh.
#  @Positive
#  Scenario: Search for product apple
#    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
#    Then he sees the results displayed for "apple"
#
#  @Positive
#  Scenario: Search for product mango
#    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
#    Then he sees the results displayed for "mango"
#
#  @Positive
#  Scenario: Search for product tofu
#    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
#    Then he sees the results displayed for "tofu"
#
#  @Positive
#  Scenario: Search for product water
#    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
#    Then he sees the results displayed for "water"



