This framework is build for API automation and for that we have used Serenity BDD Framework with java and maven.

*Pre-Requisite*

Java installed
Maven installed
Eclipse or IntelliJ installed

*This framework is consists of:*

Java
Maven
Serenity
Serenity Maven
Serenity Cucumber6
JUnit
Serenity Rest-assured
Maven Surefire Plugin
Maven Failsafe Plugin
Maven Compiler Plugin

*Steps to install project*
Download the project from GitLab https://gitlab.com/automation-qa2/serenity_api_framework
Import project in Eclipse or IntelliJ
Execute following Command in terminal "mvn install"

*Step to execute scripts locally*
Open terminal in IDE and execute command "mvn clean verify"

*Steps to view html report*
 1. Refresh the project directory
 2. Go to directory target\site\serenity
 3. Open index.html file in any browser to see the execution result

*Steps to create new test scripts*

1. Create new Scenario
    1. Go to directory \src\test\resources\features\search
    2  Create Scenario in BDD format(Refer link to know more about serenity Framework https://serenity-bdd.info/what-is-serenity/)

2. Run new created scenario(Right click on feature file and select Run option)
           *or*
   Execute Command "mvn clean verify"

3. Auto generated stepdefinitions code from missing steps will be created

4. Copy the Auto generated stepdefinations code in stepdefinitions class.
   If stepdefinitions class not present fo this feature if not created then Go to \src\test\java\starter\stepdefinitions directory and create new stepdefination class
5. Write the code in stepdefinitions methods to complete the steps
6. Verify correct path mentioned for feature file and stepdefinitions


*Refactored*

================== Change Details ===========
1. Removed folders/files related to gradle form the project directory
2. Refactored *post_product.feature* Feature file
- Added Scenario Heading
- Created 11 scenarios(9 positive scenarios for different products search)and two negative scenario.
- Replaced step *Then he sees the results displayed for apple* with *Then he sees the results displayed for "apple"*
- Create this step to reuse the same step to verify product search for all the products with passing different products name
- Created steps to validate all the products

3. Changes in SearchStepDefinitions class
- Created new setdefinition method heSeesTheResultsDisplayedFor and added assertion to verify status code and product name
  -Fixed method heDoesnNotSeeTheResults
    - Added code to assert status code in case of invalid product
    - Added code to assert error and error description

4. Renamed html report name in serenity.properties file

*Reason for 4 test scripts failure which are commented*
- For some of the Products product name is not present in response
  *Example* -
  *Expected* - Apple
  *Actual* - Jumbo Jonagold Extra Voordeel 1

*Expected* - mango
*Actual* - 8% Fat Yogurt 200 g

*Refer target\site\serenity\index.html report for more details*





